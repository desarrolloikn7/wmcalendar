json.extract! doctor, :id, :cedula, :nombre, :telefono, :created_at, :updated_at
json.url doctor_url(doctor, format: :json)