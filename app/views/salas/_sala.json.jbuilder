json.extract! sala, :id, :numero, :disponibilidad, :created_at, :updated_at
json.url sala_url(sala, format: :json)