class CreateSalas < ActiveRecord::Migration[5.0]
  def change
    create_table :salas do |t|
      t.integer :numero
      t.boolean :disponibilidad

      t.timestamps
    end
  end
end
