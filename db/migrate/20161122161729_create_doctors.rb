class CreateDoctors < ActiveRecord::Migration[5.0]
  def change
    create_table :doctors do |t|
      t.string :cedula
      t.string :nombre
      t.string :telefono

      t.timestamps
    end
  end
end
