Rails.application.routes.draw do
  devise_for :admins
  resources :salas
  resources :doctors

  root 'home#index'


  #root :to => 'devise/sessions#create'
  #get '/admins/sign_in', to: 'devise/sessions#create', as: 'login'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
